package com.protectedspaces.mapactivate;

public class Constants{
    public static final String POLICE_EMERGENCY_NUMBER = "100";
    public static final String MDA_EMERGENCY_NUMBER = "101";
    public static final String FIRE_EMERGENCY_NUMBER = "102";
    public static final String IEC_EMERGENCY_NUMBER = "103";
    public static final String HFC_EMERGENCY_NUMBER = "104";
    public static final String URBAN_CENTER = "106";
    public static final String ZAKA_CENTER= "1220";
    public static final String RESCUE_UNION = "1221";
    public static final String ERAN_EMERGENCY_NUMBER = "1201";
    public static final String ERAN_RUSSIAN_NUMBER = "1-800-24-1201";
    public static final String ERAN_AMHARIC_NUMBER = "1-800-21-1201";
    public static final String ERAN_ARABIC_NUMBER = "1-700-50-1201";
    public static final String DISTRESS_EMERGENCY_NUMBER = "118";
    public static final String VICTIMS_OF_TERRORISM = "1-800-363363";
    public static final String MENTAL_EMERGENCY_NUMBER = "03-5720825";
}
