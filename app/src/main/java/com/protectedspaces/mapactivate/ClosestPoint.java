package com.protectedspaces.mapactivate;
import android.content.Intent;
import com.mapbox.geojson.Point;
import com.mapbox.turf.TurfMeasurement;
import java.io.IOException;
import java.util.List;

public class ClosestPoint {
    GetAllLocations getAllLocations = new GetAllLocations();
    List<LocationDTO> li;

    public Point findClosestPoint(Point currentLoc) {

        try {
            li = getAllLocations.getLocations();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        double minDistanceBetweenPlaces;
        double tempDistanceBetweenPlaces;
        Point p=Point.fromLngLat(li.get(0).getY(),  li.get(0).getX());
        Point retPoint=p;
        minDistanceBetweenPlaces = TurfMeasurement.distance(currentLoc, p);
        for (LocationDTO loc : li)
        {
            p=Point.fromLngLat(loc.getX(),  loc.getY());
            tempDistanceBetweenPlaces= TurfMeasurement.distance(currentLoc, p);
            if(tempDistanceBetweenPlaces<minDistanceBetweenPlaces) {
                minDistanceBetweenPlaces = tempDistanceBetweenPlaces;
                retPoint=p;
            }
        }

        if(minDistanceBetweenPlaces>=0.300){
            return null;
        }
        return retPoint;

    }
}
