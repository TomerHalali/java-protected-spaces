package com.protectedspaces.mapactivate;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

public class EmergencyButton extends AppCompatActivity {

    @Override
    protected void onCreate (Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_emergency_button);
    }

    public boolean permissionRequest () {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission (Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions (new String[]{Manifest.permission.CALL_PHONE}, 1);
            return false;
        }
        return true;
    }

    public void BtnPolice_onClick (View view) {
        onClick (Constants.POLICE_EMERGENCY_NUMBER);
    }

    public void BtnAmbulance_onClick (View view) {
        onClick (Constants.MDA_EMERGENCY_NUMBER);
    }

    public void BtnFire_onClick (View view) {
        onClick (Constants.FIRE_EMERGENCY_NUMBER);
    }

    public void BtnPower_onClick (View view) {
        onClick (Constants.IEC_EMERGENCY_NUMBER);
    }

    public void BtnPikud_onClick (View view) {
        onClick (Constants.HFC_EMERGENCY_NUMBER);
    }

    public void BtnIroni_onClick (View view) {
        onClick (Constants.URBAN_CENTER);
    }

    public void BtnZaka_onClick (View view) {
        onClick (Constants.ZAKA_CENTER);
    }

    public void BtnUnion_onClick (View view) {
        onClick (Constants.RESCUE_UNION);
    }

    public void BtnEran_onClick (View view) {
        onClick (Constants.ERAN_EMERGENCY_NUMBER);
    }

    public void BtnEranR_onClick (View view) {
        onClick (Constants.ERAN_RUSSIAN_NUMBER);
    }

    public void BtnEranAm_onClick (View view) {
        onClick (Constants.ERAN_AMHARIC_NUMBER);
    }

    public void BtnEranAr_onClick (View view) {
        onClick (Constants.ERAN_ARABIC_NUMBER);
    }

    public void BtnDistress_onClick (View view) {
        onClick (Constants.DISTRESS_EMERGENCY_NUMBER);
    }

    public void BtnTerror_onClick (View view) {
        onClick (Constants.VICTIMS_OF_TERRORISM);
    }

    public void BtnMental_onClick (View view) {
        onClick (Constants.MENTAL_EMERGENCY_NUMBER);
    }

    private void onClick (String phoneNumber) {
        Intent intent = new Intent (Intent.ACTION_CALL);
        intent.setData (Uri.parse ("tel: " + phoneNumber));
        if (permissionRequest ()) {
            startActivity (intent);
        }
    }

}
