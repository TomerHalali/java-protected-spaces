package com.protectedspaces.mapactivate;
import android.os.StrictMode;
import android.view.View;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;

public class GetAllLocations {
    ApiInterface apiInterface;
    public GetAllLocations(){
        apiInterface = ApiClient.getClient().create(ApiInterface.class);
    }

    public List<LocationDTO> getLocations() throws IOException {
        /*
        The solution of this exception:
         Method threw 'andriod.os.NetworkOnMainThreadException' exception  */
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        Call<List<LocationDTO>> call = apiInterface.getAllLocations();
        List<LocationDTO> locationsList = call.execute().body();
        return locationsList;

    }
}
